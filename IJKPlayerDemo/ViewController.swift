//
//  ViewController.swift
//  IJKPlayerDemo
//
//  Created by leotao on 2020/11/20.
//

import UIKit
import IJKMediaFrameworkWithSSL
import SnapKit

class ViewController: UIViewController {
    var player: IJKMediaPlayback?
    var timer: Timer?
    @IBOutlet weak var slider: UISlider!
    
    lazy var smallView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.clipsToBounds = true
        
        let label = UILabel()
        label.text = "点我切换为小窗"
        label.textColor = .white
        view.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onClick))
        view.addGestureRecognizer(tap)
        
        return view
    }()
    
    lazy var playerView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .black
        
        let label = UILabel()
        label.text = "点击小窗还原"
        label.textColor = .white
        view.addSubview(label)
        
        label.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        #if true
        IJKFFMoviePlayerController.setLogReport(false)
        IJKFFMoviePlayerController.setLogLevel(k_IJK_LOG_SILENT)
        #endif
        
        self.view.backgroundColor = .white
        let urlString = "https://vfx.mtime.cn/Video/2016/11/21/mp4/161121065305521110.mp4"
        guard let url = URL(string: urlString), let option = IJKFFOptions.byDefault() else { return }
        guard let player = IJKFFMoviePlayerController(contentURL: url, with: option) else { return }
        player.scalingMode = .aspectFill
        self.player = player
        
        
        view.addSubview(playerView)
        playerView.snp.makeConstraints { (maker) in
            maker.left.right.top.equalToSuperview()
            maker.height.equalTo(playerView.snp.width).multipliedBy(9.0 / 16.0).priority(.high)
            maker.height.lessThanOrEqualToSuperview().priority(.required)
        }
        
        playerView.addSubview(player.view);
        player.view.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        view.addSubview(smallView)
        smallView.snp.makeConstraints { (maker) in
            maker.left.equalToSuperview()
            maker.top.equalTo(playerView.snp.bottom)
            maker.size.equalTo(CGSize(width: 200, height: 100))
        }
        
        addObservers()
    }
    
    @objc func onClick() {
        guard let player = self.player else { return }
        
        if player.view.superview == smallView {
            playerView.addSubview(player.view)
            
        } else {
            smallView.addSubview(player.view)
        }
        
        player.view.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.player?.prepareToPlay()
    }
    
    @IBAction func onSliderValueChanged(_ sender: UISlider) {
        guard let player = self.player else { return }
        player.currentPlaybackTime = player.duration * Double(sender.value)
    }
    
    @IBAction func onPause(_ sender: UIButton) {
        self.player?.pause()
    }
    
    @IBAction func onPlay(_ sender: UIButton) {
        self.player?.play()
    }
    
    @IBAction func onStop(_ sender: UIButton) {
        self.player?.stop()
    }
    

    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(onPlaybackStateDidChange(noti:)), name: NSNotification.Name.IJKMPMoviePlayerPlaybackStateDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onMoviePlayBackDidFinish(noti:)), name: NSNotification.Name.IJKMPMoviePlayerPlaybackDidFinish, object: nil)
    }
    
    @objc func onPlaybackStateDidChange(noti: Notification) {
        guard let player = self.player else { return }
        print(#function, player.playbackState.rawValue)
        switch player.playbackState {
        case .playing:
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (timer) in
                self?.slider.value = Float(player.currentPlaybackTime / player.duration)
                print(player.bufferingProgress)
            })
        default:
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc func onMoviePlayBackDidFinish(noti: Notification) {
        guard let reason = noti.userInfo?[IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey] as? IJKMPMovieFinishReason else {
            return
        }
        switch reason {
        case .playbackEnded:
            self.player?.currentPlaybackTime = 0
            self.player?.play()
        default:
            break
        }
    }
}

